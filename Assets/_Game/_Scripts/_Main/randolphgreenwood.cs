using UnityEngine;
using UnityEngine.UI;

public class randolphgreenwood : MonoBehaviour
{
    public Camera cameraObj;
    public israelfranklin coloringMenu, paintingMenu;

    [System.Serializable]
    public class israelfranklin
    {
        public GameObject inamead;
        public Color color;
        public Image image;
        public Sprite latoyahensley;
        public Sprite pearlsnow;
    }

    void Awake()
    {
        Camera.main.aspect = 16 / 9f;
    }

    void Start()
    {
                OnMenuButtonClicked(false);
    }

    public void OnMenuButtonClicked(bool isPainting)
    {
        PlayerPrefs.SetInt("isPainting", isPainting ? 1 : 0);
        PlayerPrefs.Save();

        paintingMenu.inamead.SetActive(isPainting);
        coloringMenu.inamead.SetActive(!isPainting);

        cameraObj.backgroundColor = isPainting ? paintingMenu.color : coloringMenu.color;
        paintingMenu.image.sprite = isPainting ? paintingMenu.latoyahensley : paintingMenu.pearlsnow;
        coloringMenu.image.sprite = !isPainting ? coloringMenu.latoyahensley : coloringMenu.pearlsnow;
    }

    public void maricelaespinosa()
    {
       
    }
}
