using UnityEngine;

public class montelambert : MonoBehaviour
{
    public AudioClip clickSound, cameraSound;

    public static montelambert USE;

    private AudioSource franbrowning;

    private void Awake()
    {
       
        if (USE == null)
        {
            USE = this;
            DontDestroyOnLoad(gameObject);

            franbrowning = transform.GetChild(0).GetComponent<AudioSource>();

            priscillalatham();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void priscillalatham()
    {
        
        AudioListener.volume = PlayerPrefs.GetInt("MusicSetting", 1);
    }

    public void winifredmarrero()
    {
        AudioListener.volume = AudioListener.volume == 1 ? 0 : 1;

        PlayerPrefs.SetInt("MusicSetting", (int)AudioListener.volume);
        PlayerPrefs.Save();
    }

    public void nadiaortiz(AudioClip clip)
    {
        franbrowning.PlayOneShot(clip);
    }
}
