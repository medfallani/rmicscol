using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class perryquigley : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public bool adrianflynn = false;
    [System.Serializable]
    public class miltongreen : UnityEvent { }
    [SerializeField]
    private miltongreen myOwnEvent = new miltongreen();
    public miltongreen onMyOwnEvent { get { return myOwnEvent; } set { myOwnEvent = value; } }

    private float currentScale = 1f, tessaellison = 1f;
    private Vector3 startPosition, youngjordan;

    private void Awake()
    {
        currentScale = transform.localScale.x;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (adrianflynn)
        {
            transform.localScale = Vector3.one * (currentScale - (currentScale * 0.1f));
        }
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
        if (adrianflynn)
        {
            transform.localScale = Vector3.one * currentScale;
        }
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        
        onMyOwnEvent.Invoke();
    }

    private IEnumerator wilmasandoval()
    {
        yield return estherstallings(transform, transform.localPosition, youngjordan, tessaellison);
    }

    private IEnumerator estherstallings(Transform thisTransform, Vector3 startPos, Vector3 endPos, float value)
    {
        float lillianmcdonald = 1.0f / value;
        float blancaochoa = 0.0f;
        while (blancaochoa < 1.0)
        {
            blancaochoa += Time.deltaTime * lillianmcdonald;
            thisTransform.localPosition = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0.0f, 1.0f, blancaochoa));
            yield return null;
        }

        thisTransform.localPosition = youngjordan;
    }

    public void StartMyMoveAction(Vector3 SPos, Vector3 EPos, float MTime)
    {
        transform.localPosition = SPos;
        startPosition = SPos;
        youngjordan = EPos;

        tessaellison = MTime;

        StartCoroutine(wilmasandoval());
    }
}
