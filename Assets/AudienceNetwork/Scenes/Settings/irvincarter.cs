using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class irvincarter : MonoBehaviour
{
    private static string erikakoenig = "URL_PREFIX";

    public InputField urlPrefixInput;
    public Text sdkVersionText;

    private string casandraterry;

    
    public static void mariannecoulter()
    {
        string prefix = PlayerPrefs.GetString(erikakoenig, "");
        AudienceNetwork.AdSettings.SetUrlPrefix(prefix);
    }

    void Start()
    {
        casandraterry = PlayerPrefs.GetString(erikakoenig, "");
        urlPrefixInput.text = casandraterry;
        sdkVersionText.text = AudienceNetwork.SdkVersion.Build;
    }

    public void OnEditEnd(string prefix)
    {
        casandraterry = prefix;
        SaveSettings();
    }

    public void SaveSettings()
    {
        PlayerPrefs.SetString(erikakoenig, casandraterry);
        AudienceNetwork.AdSettings.SetUrlPrefix(casandraterry);
    }

    public void AdViewScene()
    {
        SceneManager.LoadScene("AdViewScene");
    }

    public void InterstitialAdScene()
    {
        SceneManager.LoadScene("InterstitialAdScene");
    }

    public void RewardedVideoAdScene()
    {
        SceneManager.LoadScene("RewardedVideoAdScene");
    }
}
